# -*- coding: utf-8 -*-
# Import Section
# bokeh serve --show "Google Drive\Werkstudent\mongoTools\bokeh_new.py"
# conda install -c bokeh/channel/dev bokeh

import json
import os
import re
from collections import Counter, defaultdict
from datetime import datetime
from operator import itemgetter

from bokeh import events
from bokeh.layouts import column, row
from bokeh.models import (
    GMapPlot, GMapOptions, HoverTool, ColumnDataSource, Circle, DataRange1d, PanTool, WheelZoomTool, ResetTool, Slider,
    Button, MultiSelect, BoxZoomTool, SaveTool, MultiLine, BoxSelectTool, PreText, FixedTicker)
from bokeh.models.callbacks import CustomJS
from bokeh.palettes import Category20
from bokeh.plotting import curdoc, figure
from pymongo import MongoClient
import bokeh.embed

'''Preparation of basic Variables'''
client = MongoClient()
db = client.mongo_db
mong = db.test_collection

regex = re.compile('[^,; \-\.abcdefghijklmnopqrstuvwxyz\d]', flags=re.IGNORECASE)
brackets = re.compile('[\(\[].*?[\)\]]', flags=re.IGNORECASE)
dPrimType = mong.distinct('primary_type')
# dCounType = [brackets.sub('', regex.sub('', s)) for s in mong.distinct('primary_country.name')]

'''ColorDict'''
colorDict = {key: Category20[20][i] for i, key in enumerate(dPrimType)}


def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

'''Creating Dummy Data'''
data = {key: list() for key in
        ['class', 'title', 'country', 'type', 'primary_type', 'glide', 'desc', 'lon', 'lat', 'year', 'colors']}
CircleMapSource = ColumnDataSource(data)
fade1source = ColumnDataSource(data)
fade2source = ColumnDataSource(data)
fade3source = ColumnDataSource(data)
fade4source = ColumnDataSource(data)
data = {key: list() for key in ['count', 'primary_type', 'year', 'colors']}
CirclePlotSourc = ColumnDataSource(data)
mLinePlotSource = ColumnDataSource(data)
sourceDict = defaultdict(list)
file = os.path.join(os.path.dirname(__file__), 'world.json')
with open(file) as f:
    jsonFile = json.load(f)
    for feature in jsonFile['features']:
        for geom in feature['geometry']['coordinates']:
            if feature['geometry']['type'] == 'Polygon':
                sourceDict['xs'].append([g[0] for g in geom])
                sourceDict['ys'].append([g[1] for g in geom])
                sourceDict['name'].append(feature['properties']['geounit'])
            elif feature['geometry']['type'] == 'MultiPolygon':
                for sub in geom:
                    sourceDict['xs'].append([g[0] for g in sub])
                    sourceDict['ys'].append([g[1] for g in sub])
                    sourceDict['name'].append(feature['properties']['geounit'])

'''Preparing The Hover Boxes'''
hoverWorld = HoverTool(tooltips=[("Country", "@name"), ("Lon, Lat", "$x, $y")])
hoverGmap = HoverTool(tooltips=[("Title", "@title"), ("Lat, Lon", "(@lat, @lon)"),
                                ("Country", "@country"), ("year", "@year")])
hoverTmap = HoverTool(tooltips=[("Title", "@title"), ("Lat, Lon", "(@lat, @lon)"),
                                ("Country", "@country"), ("year", "@year")])
hoverPlot = HoverTool(tooltips=[("Year", "@year"), ("Count", "@count"), ("Type", "@primary_type")])

'''Preparing the Textbox'''
textList = '1: Welcome and have Fun!\n\n2: Errors will be displayed here.\n\n'
textBox = PreText(text=textList, height=150)


'''Preparing the World Map'''
geo_source = ColumnDataSource(sourceDict)
worldPlot = figure(plot_width=480, plot_height=240, background_fill_color='lightblue',
                   tools="tap, pan, wheel_zoom, reset", webgl=True)
worldPlot.patches(xs='xs', ys='ys', fill_color="#EBE02A", fill_alpha=1, line_alpha=0.1,
                  line_color='black', source=geo_source)
worldPlot.xaxis.axis_label = "Longitude"
worldPlot.yaxis.axis_label = "Latitude"
worldPlot.add_tools(hoverWorld)
worldPlot.toolbar_location = 'above'


'''Preparing the Google Map'''
map_options = GMapOptions(lat=0, lng=0, map_type='roadmap', zoom=3, scale_control=True)
gMap = GMapPlot(plot_width=1100, plot_height=500, x_range=DataRange1d(), y_range=DataRange1d(),
                map_options=map_options, webgl=True)
gMap.title.text = "Disaster MongoDB"
gMap.api_key = 'AIzaSyD8qe_qvnny_jUovGR86OjQQ6QF_-59Ynk'
# All The Glyps
glypMapCirc = Circle(x="lon", y="lat", size=11, fill_color="colors", fill_alpha=0.8, line_color=None)
fade1 = Circle(x="lon", y="lat", size=11, fill_color="colors", fill_alpha=0.7, line_color=None)
fade2 = Circle(x="lon", y="lat", size=11, fill_color="colors", fill_alpha=0.5, line_color=None)
fade3 = Circle(x="lon", y="lat", size=11, fill_color="colors", fill_alpha=0.3, line_color=None)
fade4 = Circle(x="lon", y="lat", size=11, fill_color="colors", fill_alpha=0.1, line_color=None)
gMap.add_glyph(CircleMapSource, glypMapCirc)
gMap.add_glyph(fade1source, fade1)
gMap.add_glyph(fade2source, fade2)
gMap.add_glyph(fade3source, fade3)
gMap.add_glyph(fade4source, fade4)
gMap.add_tools(PanTool(), BoxZoomTool(), WheelZoomTool(), hoverGmap, ResetTool(), SaveTool())
'''Preparing the Tile Provider Map'''
# bound = 20000000 # meters
# tileMap = figure(tools='pan, wheel_zoom, box_zoom, reset', x_range=(-bound, bound), y_range=(-bound, bound), webgl=True)
# tileMap.add_glyph(CircleMapSource, glypMapCirc)
# tileMap.add_tools(hoverTmap)
# tileMap.axis.visible = False
# tileMap.add_tile(STAMEN_TERRAIN)


'''Preparing The Line Plot'''
plot = figure(x_range=DataRange1d(), y_range=DataRange1d(), plot_width=600, plot_height=240,
              toolbar_location='left', y_axis_type="log", webgl=True)  # , background_fill_color='#F8ECC2')
plot.xaxis.axis_label = "Year"
plot.yaxis.axis_label = "Occurences"
# plot.add_layout(LinearAxis(axis_label='Latitude'), 'right')
plot.yaxis[0].ticker = FixedTicker(ticks=[1, 5, 10, 50, 100, 500])
glyphCircle = Circle(x='year', y='count', fill_color='colors', line_color=None, fill_alpha=0.6)
plot.add_glyph(CirclePlotSourc, glyphCircle)
glyphLine = MultiLine(xs='year', ys='count', line_color='colors', line_alpha=0.4)
plot.add_glyph(mLinePlotSource, glyphLine)
plot.add_tools(hoverPlot, BoxSelectTool())  # , WheelZoomTool(), BoxZoomTool(), PanTool(), ResetTool())
plot.toolbar_location = 'above'


'''Starting the Widgets'''
animButton = Button(label=' > Play')
sDict = dict()
sDict['sY'] = Slider(start=1980, end=datetime.now().year, step=1, callback_policy='mouseup',
                     title='Starting Year', value=2000)
sDict['sM'] = Slider(start=1, end=12, step=1, callback_policy='mouseup',
                     title='Starting Month', value=1)
sDict['eY'] = Slider(start=1980, end=datetime.now().year, step=1, callback_policy='mouseup',
                     title='Ending Year', value=datetime.now().year)
sDict['eM'] = Slider(start=1, end=12, step=1, callback_policy='mouseup',
                     title='Ending Month', value=3)
m = MultiSelect(title="Type Options:", value=dPrimType, size=len(dPrimType),
                options=list(zip(dPrimType, [typ.title() for typ in dPrimType])))

# tab1 = Panel(child=gMap, title='GoogleMaps Basemap')
# tab2 = Panel(child=tileMap, title='Terrain Basemap')
# tabs = Tabs(tabs=[tab1, tab2])

'''Defining the Callbacks to change the Source Data and do the mongodb Request'''


def plot_select_callback(attr, old, new):
    if new is not None:
        print(new['1d']['indices'])
        print(new['1d'])
        m.value = [CirclePlotSourc.data['primary_type'][idx] for idx in new['1d']['indices']]
        sDict['sY'].value = min([CirclePlotSourc.data['year'][idx] for idx in new['1d']['indices']])
        sDict['sM'].value = 12
        sDict['eY'].value = max([CirclePlotSourc.data['year'][idx] for idx in new['1d']['indices']]) + 1
        sDict['eM'].value = 1


def world_select_callback(attr, old, new):
    if geo_source.selected['1d']['indices']:
        gMap.map_options.lng = mean(geo_source.data['xs'][geo_source.selected['1d']['indices'][0]])
        gMap.map_options.lat = mean(geo_source.data['ys'][geo_source.selected['1d']['indices'][0]])
        gMap.map_options.zoom = 4
    else:
        gMap.map_options.lng = 0
        gMap.map_options.lat = 0
        gMap.map_options.zoom = 2
    callback(None, None, None)
    pass


def callback(attr, old, new):
    # BEST PRACTICE --- update .data in one step with a new dict
    # Loading the Data from Mongo and set it as gMap DataSource
    new_data = defaultdict(list)
    '''Loading the Data from Mongo and set it as gMap DataSource'''
    if m.value:
        searchDict = {'datetime': {
            '$gte': datetime(sDict['sY'].value, sDict['sM'].value, 1),
            '$lte': datetime(sDict['eY'].value, sDict['eM'].value, 1)},
            'primary_type': {'$in': m.value}, 'class': 'event'}
        if geo_source.selected['1d']['indices']:
            searchDict['country'] = \
                {'$elemMatch': {'name': {'$in': [re.compile(c, re.IGNORECASE)
                                                 for c in [geo_source.data['name'][idx]
                                                           for idx in geo_source.selected['1d']['indices']]]}}}
        results = mong.find(searchDict)
        if not results.count():
            textBox.text = textBox.text[textBox.text.find('\n\n')+2:]
            textBox.text += '%i: No entrys Found!!\n\n' % (int(textBox.text[0]) + 1)
            return
        for entry in results:
            for country in entry['country']:
                new_data['class'].append(entry.get('class', None))
                new_data['title'].append(regex.sub('', str(entry['title']))[:59].strip())
                new_data['country'].append(regex.sub('', str(country['name']))[:29].strip())
                new_data['type'].append(regex.sub('', str(entry['type']))[:199].strip())
                new_data['primary_type'].append(entry['primary_type'])
                new_data['year'].append(entry['datetime'].date().year)
                new_data['glide'].append(regex.sub('', str(entry.get('glide', None))).strip())
                new_data['desc'].append(
                    regex.sub('', str(entry.get('information',
                                                [{'description': None}])[0].get('description', None)))[:199].strip()
                )
                new_data['lon'].append(country['location']['lon'])
                new_data['lat'].append(country['location']['lat'])
                new_data['colors'].append(colorDict[entry['primary_type']])

        # BEST PRACTICE --- update .data in one step with a new dict -- Done
        CircleMapSource.data = new_data

        '''Add the Count for the DataPlot Circles and Lines'''
        c = Counter(['%s%s' % (year, pType) for year, pType in zip(new_data['year'], new_data['primary_type'])])
        new_data['count'] = [c['%s%s' % (year, pType)] for year, pType
                             in zip(new_data['year'], new_data['primary_type'])]

        '''Reduce the amount of Data to fit the plot (1 per year and type)'''
        smaller_data = defaultdict(list)
        for line in zip(new_data['count'], new_data['primary_type'], new_data['year'], new_data['colors']):
            ident = '%s%s' % (line[1], line[2])
            if ident not in smaller_data['ident']:
                line = list(line)
                line.append(ident)
                for key, value in zip(['count', 'primary_type', 'year', 'colors', 'ident'], line):
                    smaller_data[key].append(value)

        new_data = smaller_data.copy()
        '''Sort them By Primary Type and Year'''
        idxPType, idxYear = list(new_data.keys()).index('primary_type'), list(new_data.keys()).index('year')
        new_data = {key: value for key, value in
                    zip(new_data.keys(),
                        zip(*sorted(zip(*new_data.values()), key=itemgetter(idxPType, idxYear), reverse=True)))
                    }
        super_new_data = defaultdict(list)
        for primType in set(new_data['primary_type']):
            super_new_data['count'].append(
                new_data['count'][
                    new_data['primary_type'].index(primType):
                    len(new_data['primary_type']) - new_data['primary_type'][::-1].index(primType)
                ])
            super_new_data['year'].append(
                new_data['year'][
                    new_data['primary_type'].index(primType):
                    len(new_data['primary_type']) - new_data['primary_type'][::-1].index(primType)
                ])
            super_new_data['colors'].append(new_data['colors'][new_data['primary_type'].index(primType)])
            super_new_data['primary_type'].append(primType)
        mLinePlotSource.data = super_new_data
        super_new_data['year'] = [x for y in super_new_data['year'] for x in y]
        super_new_data['count'] = [x for y in super_new_data['count'] for x in y]
        CirclePlotSourc.data = new_data
    else:
        textBox.text = textBox.text[textBox.text.find('\n\n')+2:]
        textBox.text += '%i: Select a Type!\n\n' % (int(textBox.text[0]) + 1)
        print('Select a Type!')


def mouse_event(attributes=list()):
    """
    Function that returns a Python callback to pretty print the events.
    """
    print('tap detected')

    def python_callback(event):
        cls_name = event.__class__.__name__
        attrs = ', '.join(['{attr}={val}'.format(attr=attr, val=event.__dict__[attr])
                           for attr in attributes])
        print('{cls_name}({attrs})'.format(cls_name=cls_name, attrs=attrs))
    return python_callback


def animate():
    fade1source.data = defaultdict(list)
    fade2source.data = defaultdict(list)
    fade3source.data = defaultdict(list)
    fade4source.data = defaultdict(list)
    if animButton.label == ' > Play':
        animButton.label = 'II Pause'
        curdoc().add_periodic_callback(animate_update, 200)
    else:
        animButton.label = ' > Play'
        curdoc().remove_periodic_callback(animate_update)
        callback(None, None, None)


def animate_update():

    month = sDict['sM'].value + 1
    if month > sDict['sM'].end:
        month = sDict['sM'].start
        year = sDict['sY'].value + 1
        sDict['sY'].value = sDict['sY'].start if year > sDict['sY'].end else year
        sDict['eY'].value = sDict['eY'].start if year > sDict['sY'].end else year
    sDict['sM'].value = month
    month += 1
    sDict['eM'].value = sDict['eM'].start if month > sDict['eM'].end else month
    fade4source.data = fade3source.data
    fade3source.data = fade2source.data
    fade2source.data = fade1source.data
    fade1source.data = CircleMapSource.data
    callback(None, None, None)


'''Activate the callbacks'''

fakeDict = dict()
for key in sDict.keys():
    # Fake Callback for Slider Response:
    fakeDict[key] = ColumnDataSource(data=dict(value=list()))
    fakeDict[key].on_change('data', callback)
    sDict[key].callback = CustomJS(args=dict(source=fakeDict[key]),
                                   code="""source.data = { value: [cb_obj.value] }""")

m.on_change('value', callback)
animButton.on_click(animate)
CirclePlotSourc.on_change('selected', plot_select_callback)
geo_source.on_change('selected', world_select_callback)
worldPlot.on_event(events.Tap, mouse_event(attributes=['x', 'y', 'sx', 'sy']))
callback(None, None, None)


'''''''''XXXXXXX''''STARTUP''''XXXXXXXXX'''''''''''
curdoc().add_root(column(row(m, worldPlot, plot),
                         row(column(sDict['sY'], sDict['sM'], sDict['eY'], sDict['eM'], animButton, textBox), gMap)))
curdoc().title = "World Disaster Database"