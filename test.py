# import numpy as np

# from bokeh.plotting import figure, curdoc
# from bokeh.embed import components
# from bokeh.embed import file_html
# from bokeh.resources import CDN

# N = 4000
# x = 100
# y = 100

# plot = figure()
# plot.circle(x, y, fill_alpha=0.6, line_color=None)

# plot_script, plot_div = components(plot)

# print('\tplot_script: {}\n\tplot_div: {}'.format(plot_script, plot_div))

# with open('./bla.txt', 'w') as f:
#     f.write(plot_script)
#     f.write('\n\n\n')
#     f.write(plot_div)

# html = file_html(plot, CDN, template="./templateWebsite/index.html")

from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import file_html, autoload_server
from bokeh.io import show, save

plot = figure()
plot.circle([1,2], [3,4])

html = file_html(plot, CDN, title="Test", template="./templateWebsite/index.html")

open('./test.html', 'w').write(html)
