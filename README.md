# World Disaster Map

## Design

![](./templateWebsite/Design/Design.png)

## Problemstellung

Die von Bokeh erstellten Diagramme sollen mit einer [Template-Webseite](./templateWebsite) gerendert werden.

## Versuche der Problemlösung

### `file_html`

Mit Hilfe von `file_html` sollen die Diagramme integriert werden

```python
from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import file_html, autoload_server
from bokeh.io import show, save

plot = figure()
plot.circle([1,2], [3,4])

html = file_html(plot, CDN, title="Test", template="./templateWebsite/index.html")

open('./test.html', 'w').write(html)
```

Error:

```bash
> python .\test.py
Traceback (most recent call last):
  File ".\test.py", line 34, in <module>
    html = file_html(plot, CDN, title="Test", template=open("./templateWebsite/index.html", 'r').read())
  File "C:\Users\LC\AppData\Local\Programs\Python\Python35\lib\site-packages\bokeh\embed.py", line 433, in file_html
    template=template, template_variables=template_variables)
  File "C:\Users\LC\AppData\Local\Programs\Python\Python35\lib\site-packages\bokeh\embed.py", line 667, in _html_page_for_render_items
    html = template.render(template_variables_full)
AttributeError: 'str' object has no attribute 'render'
```

Desweiteren erscheint nur eine leere Seite.